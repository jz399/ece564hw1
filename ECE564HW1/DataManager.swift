//
//  DataManager.swift
//  ECE564HW1
//
//  Created by jz399 on 1/19/23.
//

import Foundation

class DataManager {
    static let dm = DataManager()  // singleton pattern
    private var dict = [String:DukePerson]()
    
    private init() {}
    
    /// Try to add an new entry to the database. If the entry does not exist, add it and
    /// return true. Otherwise return false
    func addEntry(entry: DukePerson) -> Bool{
        let key = entry.netID
        if dict[key] != nil{
            return false
        }else{
            dict[key] = entry
            return true
        }
    }
    
    /// Try to delete an entry in the database. If the entry exists, delete it and
    /// return true. Otherwise return false
    func deleteEntry(key: String) -> Bool{
        if(dict[key] != nil){
            dict[key] = nil
            return true
        }else{
            return false
        }
    }
    
    /// Find the DukePerson instance based on key. If entry exists, return the instance.
    /// Otherwise return nil
    func findEntry(key: String) -> DukePerson?{
        if(dict[key] != nil){
            return dict[key]
        }else{
            return nil
        }
    }
    
    /// Return all the entry in database.
    func getAllEntry() -> [DukePerson]{
        var arr: [DukePerson] = []
        for entry in dict{
            arr.append(entry.value)
        }
        return arr
    }
}
