//
//  DataModel.swift
//  ECE564HW1
//
//  Created by jz399 on 1/19/23.
//

import Foundation


/// If the "newPerson.netid" is not already in the database, add newPerson to the
/// database and return true. Otherwise return false.
func addPerson(_ newPerson: DukePerson) -> Bool {
    return DataManager.dm.addEntry(entry: newPerson)
}

/// Looks for the "updatedPerson.netid" in the database. If found, replaces that
/// entry, and returns true. If not found, then adds "updatedPerson" to database and
/// returns false.
func updatePerson(_ updatedPerson: DukePerson) -> Bool {
    let res = DataManager.dm.deleteEntry(key: updatedPerson.netID)
    _ = DataManager.dm.addEntry(entry: updatedPerson)
    return res
}

/// Looks for "netID" entry in database. If found, remove the entry from the
/// database and return true. If not found, return false.
func deletePerson(_ netID: String) -> Bool {
    return DataManager.dm.deleteEntry(key: netID)
}

/// Searches for an entry with "netID". If found, returns the info as an Optional
/// DukePerson. If not found, returns nil.
func findPerson(_ netID: String) -> DukePerson? {
    return DataManager.dm.findEntry(key: netID)
}

/// Finds all the people in the database that match the given lastName and
/// firstName. The firstName parameter is defaulted, so you can also call the method with
/// just the lastName.
func findPeople(lastName lName: String, firstName fName: String = "*") -> [DukePerson]? {
    var res: [DukePerson] = []
    let all = DataManager.dm.getAllEntry()
    for entry in all{
        if(entry.lname == lName){
            res.append(entry)
        }
    }
    return res.isEmpty ? nil : res
}

/// Returns an array of all the people in the database
func listPeople() -> [DukePerson] {
    return DataManager.dm.getAllEntry()
}
