//
//  DataStructures.swift
//  ECE564HW1
//
//  Created by jz399 on 1/19/23.
//

import Foundation

enum DukeRole : String {
 case Professor = "Professor"
 case TA = "TA"
 case Student = "Student"
 case Other = "Other"
}

class DukePerson : CustomStringConvertible{
    var fname: String
    var lname: String
    var from: String
    var hobby: String
    var movie: String
    var gender: Int
    var progLang: [String]
    var role: DukeRole
    var netID: String
    
    var description: String{
        var str: String = ""
        str += fname + " " + lname + " is from " + from + " "
        str += "and is a " + role.rawValue + "."
        
        if(gender == 2){
            str += " Her best programming languages are "
        }else{
            str += " His best programming languages are "
        }
        for l in progLang{
            str += l + ", "
        }
        
        if(progLang.isEmpty){
            str += " ."
        }else{
            str.removeLast(2)
            str += "."
        }
        
        if(gender == 2){
            str += " Her favorite hobby is "
        }else{
            str += " His favorite hobby is "
        }
        str += hobby + " and "
        
        if(gender == 2){
            str += "her favorite movie is "
        }else{
            str += "his favorite movie is "
        }
        str += movie + ". "
        
        str += "you can reach "
        if(gender == 2){
            str += "her at "
        }else{
            str += "him at "
        }
        str += netID + ".duke.edu."
        
        return str
    }
    
    init() {
        self.fname = " "
        self.lname = " "
        self.from = " "
        self.hobby = " "
        self.movie = " "
        self.gender = 0
        self.progLang = []
        self.role = DukeRole.Other
        self.netID = " "
    }
}
