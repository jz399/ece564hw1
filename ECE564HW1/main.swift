//
//  main.swift
//  ECE564HW1
//
//  Created by jz399 on 1/19/23.
//

import Foundation

print("Welcome to use. Please enter the command:")

print("----------------Example--------------")
print("add fname=Ric lname=Telford netID=125 hobby=golf movie=harry-potter from=Durham,NC gender=1 proLang=C,C++ role=TA")
print("update fname=Ric lname=Telford netIDadd fname=Ric lname=Telford netID=jz399 hobby=golf movie=harry-potter from=Durham,NC=rt113 hobby=tennis...")
print("delete netID=rt113")
print("find netID=rt113")
print("find lname=Telford")
print("list")
print("quit")
print("-------------------------------------")

while true
{
    print("Please input command:")
    let input: String! = readLine()
    
    let parseRes: [Substring] = input.split(separator: " ")
    if parseRes.isEmpty{
        print("Empty input! Please input again.")
        continue
    }
    do{
        switch String(parseRes[0]){
        case "add":
            let person: DukePerson = try parseUpdateCommand(parseRes:parseRes)
            if addPerson(person) == true{
                print("Add successfully")
            }else{
                print("Instance already exists in database.")
            }
        case "update":
            let person: DukePerson = try parseUpdateCommand(parseRes:parseRes)
            if updatePerson(person) == true{
                print("update successfully")
            }else{
                print("Instance does not exist in database. Already add it!")
            }
        case "delete":
            let key = try parseDeleteCommand(parseRes:parseRes)
            if deletePerson(key) == true{
                print("delete successfully")
            }else{
                print("Instance does not exist in database.")
            }
        case "find":
            print("Result: ")
            let (argName, argValue) = try parseFindCommand(parseRes:parseRes)
            if argName == "netID"{
                let res: DukePerson? = findPerson(argValue)
                guard let res_val = res else{
                    print("Instances are not found.")
                    break
                }
                print(res_val.description)
            }else{
                let res: [DukePerson]? = findPeople(lastName: argValue)
                guard let res_val = res else{
                    print("Instances are not found.")
                    break
                }
                for p in res_val{
                    print("fname: " + p.fname + " lname: " + p.lname)
                }
            }
        case "list":
            print("Result:")
            try parseListCommand(parseRes: parseRes)
            let allEntries = listPeople()
            for entry in allEntries{
                print("fname: " + entry.fname + " lname: " + entry.lname)
            }
        case "quit":
            exit(0)
        default:
            print("Wrong command. Please input again.\n")
        }
    }catch{
        print("Please check format and input again.\n")
    }
}

