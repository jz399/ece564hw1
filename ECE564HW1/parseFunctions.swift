//
//  parseFuns.swift
//  ECE564HW1
//
//  Created by jz399 on 1/20/23.
//

import Foundation

enum ParserError: Error {
    case invalidArgFormat(arg: String)
    case invalidArgName(argName: String, argValue: String)
    case invalidArgValue(argName: String, argValue: String)
}

/// Parse the argument, which is format in "Name=Value"
/// Throw error for the wrong format
/// - Parameter arg: one argument in the command
/// - Throws: ParserError.invalidArgFormat
/// - Returns: (argName: String, argValue: String)
func parseArg(arg: String) throws->(argName: String, argValue: String){
    let parseRes:[Substring] = arg.split(separator: "=", maxSplits: 1)
    if parseRes.count != 2{
        print("wrong argument format: " + arg + ".")
        throw ParserError.invalidArgFormat(arg: arg)
    }
    return (String(parseRes[0]), String(parseRes[1]))
}

/// Parse argument value for gender property
/// Only valid for 0,1,2,3.
/// - Parameter argValue
/// - Throws: ParserError.invalidArgValue
/// - Returns: gender: Int
func parseGender(argValue: String) throws -> Int{
    let gender: Int? = Int(argValue)
    guard let gender_val = gender, (gender_val >= 0 && gender_val < 4) else{
        print("invalid value for gender.")
        throw ParserError.invalidArgValue(argName: "gender", argValue: argValue)
    }
    return gender_val
}

/// Transfer String to DukeRole instance
/// - Parameter argValue
/// - Returns: a DukeRole instance
func parseRole(argValue: String) -> DukeRole{
    switch argValue{
    case "Professor", "professor":
        return DukeRole.Professor
    case "TA", "ta":
        return DukeRole.TA
    case "Student", "student":
        return DukeRole.Student
    default:
        return DukeRole.Other
    }
}

/// Parse argument value for programming language
/// Input format should be "C,C++,swift"
/// Output: ["C", "C++", "swift"]
/// - Parameter argValue
/// - Throws: PaserError.invalidArgValue
/// - Returns: ProgLang: [String]
func parseProLang(argValue: String) throws -> [String]{
    let t: [Substring] = argValue.split(separator: ",")
    if t.isEmpty{
        print("invalid value for programming Language.")
        throw ParserError.invalidArgValue(argName: "Programming Language", argValue: argValue)
    }
    var res:[String] = []
    for item in t{
        res.append(String(item))
    }
    
    return res
}

/// Set the property for given DukePerson instance,
/// throw error for non-existed propertie and the wrong argument value
/// - Parameters:
///   - obj: DukePerson instance that need to be updated
///   - argName: propertie's  name
///   - argValue: propertie's  value
/// - Throws: PaserError.invalidArgName
func setDukePersonProperty(obj: DukePerson, argName: String, argValue: String) throws {
    switch argName{
    case "fname":
        obj.fname = argValue
    case "lname":
        obj.lname = argValue
    case "from":
        obj.from = argValue
    case "hobby":
        obj.hobby = argValue
    case "movie":
        obj.movie = argValue
    case "gender":
        obj.gender = try parseGender(argValue: argValue)
    case "proLang":
        obj.progLang = try parseProLang(argValue: argValue)
    case "role":
        obj.role = parseRole(argValue: argValue)
    case "netID":
        obj.netID = argValue
    default:
        print("Property: " + argName + " does not exist.")
        throw ParserError.invalidArgName(argName: argName, argValue: argValue)
    }
}

/// Parse input command and generate an instance of
/// DukePerson based on parsed results.
/// - Parameter parseRes: Input command
/// - Throws: ParserError
/// - Returns: Generated DukePerson instance
func parseUpdateCommand(parseRes:[Substring]) throws -> DukePerson{
    let p: DukePerson = DukePerson()
    for arg in parseRes[1...]{
        let (argName, argValue) = try parseArg(arg: String(arg))
        try setDukePersonProperty(obj: p, argName: argName, argValue: argValue)
    }
    return p
}


/// Parse delete command and return netID
/// - Parameter parseRes
/// - Throws: ParserError.invalidArgFormat
/// - Returns: netID
func parseDeleteCommand(parseRes:[Substring]) throws -> String{
    if parseRes.count != 2 {
        print("wrong format for delete command")
        throw ParserError.invalidArgFormat(arg: "delete command")
    }
    let (argName, argValue) = try parseArg(arg: String(parseRes[1]))
    if argName != "netID" {
        print("Argument: " + argName + " does not exist in delete command.")
        throw ParserError.invalidArgFormat(arg: String(parseRes[1]))
    }
    return argValue
}

/// Parse find command. Find command only accepts netID or lname argument.
/// - Parameter parseRes
/// - Throws: ParserError.invalidArgFormat
/// - Returns: (argName, argValue)
func parseFindCommand(parseRes:[Substring]) throws -> (argName: String, argValue: String) {
    if parseRes.count != 2 {
        print("wrong format for find command")
        throw ParserError.invalidArgFormat(arg: "find command")
    }
    let (argName, argValue) = try parseArg(arg: String(parseRes[1]))
    if argName != "netID" && argName != "lname" {
        print("Argument: " + argName + " does not exist in find command.")
        throw ParserError.invalidArgFormat(arg: String(parseRes[1]))
    }
    return (argName, argValue)
}

/// Parse list command.
/// - Parameter parseRes
/// - Throws: ParserError.invalidArgFormat
func parseListCommand(parseRes:[Substring]) throws {
    if parseRes.count != 1 {
        print("too many argument for list command")
        throw ParserError.invalidArgFormat(arg: "list")
    }
}
